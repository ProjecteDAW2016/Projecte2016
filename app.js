var mongoose =  require('mongoose'),
    dades = require('./model/dades.js'),
    instant = require('./model/dades2.js'),
    json_perdut = require('./model/dades3.js'),
    fs = require('fs'),
    sys = require('sys'),
    http = require('http'),
    exec = require('child_process').spawn,//exec,
    // Ruta per a Raspberry
    txt = '/home/pi/Desktop/Weather/Projecte2016/model/input.txt',
    // Ruta per a equip
    //txt = './model/input.txt',
    //Ruta per a connectar amb MongoLab
    connection_string = 'mongodb://admin:admin@ds013280.mlab.com:13280/internetdelascosas',
    //variables per a controlar cada quant s'executa, el primer si hi no detecta el fitxer, espera 1 minut per tornar a provar
    //si tot va bé, el segon s'encarrega de passar el node cada 10 minuts
    waitime = 60000,
    waitload = 600000,
    //cada 15 dies, s'esborraren els registres a locals
    flush_reg = 60000 * 3600 * 15,
    //Variables per a controlar la connexió Online i la falla de dades pujades
    perdut = true,
    connect = true,
    nolab = false;

//La funció start s'executa al aixecar al servidor i es va iniciant diversos cops , cridades per altres funcions depen les ocasions
//Si no hi ha cap problema, només es crida cada 10 minuts

function start() {
    nolab = false;
    mongoose.connection.close(function (err) {
        if (err) { throw err; }
        else mongoose.connect('mongodb://localhost/dades', function (err, res) {
            if (err) {
                console.log('ERROR LOCAL: ' + err);
                start();
            } else {
                flush();
                console.log('Conexió correcta a Local');
                //Quan inicia el servidor s'encarrega de comprovar de que no hi hagi cap dada perduda a la base local
                if (perdut == true && connect == true ) {
                    comprova();
                }
                else {
                    if(perdut == false ){
                        //En aquí la db online s'ha actualitzada, així que esborrem la col·lecció
                        json_perdut.remove({},function(err){
                            if(err) console.log(err);
                            exist();
                        });
                        //La funció exist ens porta al 2n pas de l'execució, on comprova que existeixi el fitxer "input.txt"
                    } else exist();
                }
            }
        });
    });
}

//Funció per a comprovar si hi ha jsons "perduts" per error de connexió
function comprova(texts) {
    json_perdut.find(function(err, dada){
        if(err) { throw err; }
        if(dada.length === 0) {
            //Si no hi ha cap fitxer no pujat a Online, activarem la flag corresponent i tornarem al punt de partida
            console.log("No hi ha perduts!");
            perdut = false;
            start();
        } else {
            //Si hi ha jsons no pujats, tancarem connexió local per accedir al servidor
            mongoose.connection.close(function (err) {
                //  if (err) { throw err; }
                mongoose.connect(connection_string, function(err, res) {
                    if (err) {
                        //Si la connexió falla, tornarem a començar el procés i no tornarem a entrar fins que hi hagi accés a MongoLab
                        console.log('ERROR AMB CONNEXIÓ MONGOLAB SYNC: ' + err);
                        connect = false;
                        start();
                    } else {
                        console.log('Conexió correcta a MongoLab SYNC');
                        //Si ha ha connexió, ens quedarem en aquest AutoLoop, inserint totes les dades que hi hagi
                        function insertar(i,perduts,ultim) {
                            console.log(i);
                            //Depén d'on cridem la funció comprova, es passa una dada concreta o no, així que la tindrem en compte
                            //Aquesta dada es la última inserció que es disposava a fer
                            if( i < perduts.length || (i == perduts.length && typeof ultim !== 'undefined' )) {
                                if ( i < perduts.length ) text = perduts[i];
                                else if( typeof ultim !== 'undefined') console.log(ultim); //text = ultim;
                                var dada = new dades({temp: text.temp , hum_ambient: text.hum_ambient , velocitat_vent: text.velocitat_vent , hum_terra: text.hum_terra, data: [{ dia: text.data[0].dia, mes: text.data[0].mes, any: text.data[0].any, hora: text.data[0].hora, minut: text.data[0].minut}], date: new Date()});
                                new dada.save(function(err,dades) {
                                    if (!err) {
                                        console.log('Actualitzacio de MongoLab correcta per canal 2 !\n'  + dades);
                                        insertar(i+1,perduts,ultim);
                                    } else {
                                        console.log('MongoLab no ha estat actualitzada!' + err);
                                    }
                                });
                            } else {
                                start();
                                connect = true;
                                perdut = false;
                            }
                        }
                        insertar(0,dada,texts);
                    }
                });
            });
        }
    });
}

//Llegeix el fitxer on es troben les dades principals i en crea d'altres
function scan() {
    var text = fs.readFileSync(txt).toString().split(";");
    //esborra el fitxer en qüestió per a no repetir dades, i espera a l'actualització del fitxer per part del script
    //fs.unlinkSync(txt);
    //Crearem variables amb valors random per representar la velocitat del vent i la humitat de la terra
    vent = Math.floor(Math.random() * (60 - 8) + 8);
    if ((Math.random() * (50 - 0)) > 40) { vent += Math.floor(Math.random() * 30); }
    text.push(vent);
    var hum_land = Math.floor(Math.random() * 100);
    text.push(hum_land);
    text.push(new Date().getDate(),new Date().getMonth() + 1,new Date().getFullYear(),new Date().getHours(),new Date().getMinutes());
    insert(text);
}

//Copia a una col·leccio diferents els json que no s'han pujat al nuvol
function copia(text) {
    var dada = new json_perdut ({temp: text[0] , hum_ambient: text[1] , velocitat_vent: text[2] , hum_terra: text[3], data: [{ dia: text[4], mes: text[5], any: text[6], hora: text[7], minut: text[8]}]});
    mongoose.connect('mongodb://localhost/dades', function (err, res) {
        new dada.save(function(err,dades) {
            if(!err) {
                console.log('Introduit a db opcional');
                connect = false;
                perdut = true;
                setTimeout(function(){start()},waitload);
            }
        });
    });
}

function exist() {
    if (fs.existsSync(txt)) {
        scan();
    } else {
        console.log("Input.txt no existeix, esperant per tornar a carregar");
        setTimeout( function(){exist()} , waitime);
    }
}

//La funció insertar s'encarregar d'introduir les dades a local, una vegada insertades s'intenta connectar a MongoLab
//Si falla s'introduira a la Base Local una copia del Json
//Si funciona correctament, s'encarregarà de comprovar si hi ha dades perdudes hi anirà a la funció corresponent
//En el cas contrari, seguirà el procés
function insert(text) {
    var dada = new dades ({temp: text[0] , hum_ambient: text[1] , velocitat_vent: text[2] , hum_terra: text[3], data: [{ dia: text[4], mes: text[5], any: text[6], hora: text[7], minut: text[8]}]});
    new dada.save(function(err,dades) {
        if (!err) {
            console.log("Dades introduides a local correctament!\n" + dades);
            mongoose.connection.close(function (err) {
                if (err) throw err;
                mongoose.connect(connection_string, function(err) {
                    if (err) {
                        console.log('ERROR AMB CONNEXIÓ MONGOLAB: ' + err);
                        copia(text);
                        nolab = true;
                    } else {
                        if(nolab == true) mongoose.disconnect();
                        else {
                            console.log('Conexió correcta a MongoLab');
                            if ( perdut == false ) insertLab(text,0);
                            else comprova(text);
                        }
                        connect = true;
                    }
                    return;
                });
            });
        } else {
            console.log("No s'ha actualitzat correctament, tornant a iniciar");
            start();
        }
    });
}

//La funció insertLab s'encarrega d'introduir les dades a locals
//Depén quina funció la cridi escollirà un esquema o un altre, e introduirà en conseqüencia a una col·lecció o una altre
function insertLab(text,n,func) {
    var total = n;
    if( n == 0 ) {
        var dada = new dades ({temp: text[0] , hum_ambient: text[1] , velocitat_vent: text[2] , hum_terra: text[3], data: [{ dia: text[4], mes: text[5], any: text[6], hora: text[7], minut: text[8]}], date : new Date()});
        setTimeout(function(){start()},waitload);
    } else if ( n == 1 )  {
        var dada = new instant ({temp: text[0] , hum_ambient: text[1] , velocitat_vent: text[2] , hum_terra: text[3], data: [{ dia: new Date().getDate(), mes: (new Date().getMonth() + 1), any: new          Date().getFullYear(), hora: new Date().getHours(), minut: new Date().getMinutes() }],date : new Date()});
    }
    new dada.save(function(err,dades) {
        if (!err) {
            console.log('Actualitzacio de MongoLab correcta per canal ' + n + ' !\n'  + dades);
        } else {
            console.log('MongoLab no ha estat actualitzada!' + err);
        }
    });
}

//flush, s'encarrega d'alliberar espai a MongoLocal
function flush() {
    dades.count(function( err, count){
        if(count > flush_reg ){
            mongoose.connection.db.dropDatabase(function(err){
                console.log( "Copia Local esborrada!");
            })
        }});
}

//Funció listener per escoltar dades en temps real
//Primer s'assegurar d'estar connectat a Online, si no hi és, continuarà executant-se fins que hi sigui
//Una vegada connectat executarà l'script "manualment" e introduira les dades a una col·lecció que s'esborra a cada ús
http.createServer(function(request,response) {
    if (request.method=="GET" && request.url=='/') {
        function current_db(){
            mongoose.connection.db.listCollections().toArray(function (err, names) {
                if(names[2].name != "instants") current_db();
                else {
                    mongoose.connection.db.dropCollection('instants',function(err){
                        if(err) console.log("Error a instants!: " + err);
                        //executem l'script per a generar el fitxer input
                        var process = exec('sudo',["/home/pi/Desktop/Weather/Adafruit_Python_DHT/examples/AdafruitDHT.py",2302,4]);
                        //Agafem la ultima dada de velocitat_vent, ja que aquesta es medeix cada 10 minuts minim
                        setTimeout(function(){dades.findOne({},{velocitat_vent:1,_id:0},{sort: {date:-1}},function(err, dada){
                            var text = fs.readFileSync(txt).toString().split(";");
                            //esborra el fitxer en qüestió per a no repetir dades, i espera a l'actualització del fitxer per part del script
                            //fs.unlinkSync(txt);
                            //Crearem variables amb valors random per representar la velocitat del vent i la humitat de la terra
                            text.push(dada.velocitat_vent);
                            var hum_land = Math.floor(Math.random() * 100);
                            text.push(hum_land);
                            insertLab(text,1);
                        })},4000);
                    });
                }
            });
        }
        current_db();
    }
}).listen(8000);

start();
