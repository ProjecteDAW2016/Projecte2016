var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var schema = new Schema({
	temp: Number,
    hum_ambient:Number,
	velocitat_vent: Number,
    hum_terra: Number,
    data: [{ dia: String, mes: String, any: String, hora: String, minut: String }],
    date : {type: Date, default: Date.now},
},{
    versionKey: false // You should be aware of the outcome after set to false
});

module.exports = mongoose.model('dades', schema);


